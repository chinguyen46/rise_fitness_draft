const User = require("../models/user.js"),
	  Review = require("../models/review.js")

var middleware = {};

//MIDDLEWARE TO CHECK IF USER IS LOGGED IN
middleware.isUserLoggedIn = (req, res, next) =>{
	if(req.isAuthenticated()){
		return next();
	}
	req.flash("error", "Error - You need to be logged in to do that.");
	res.redirect("/login");
};

//MIDDLEWARE TO CHECK IF USER IS A PT
middleware.checkIfPersonalTrainer = (req, res, next) =>{
	if(req.isAuthenticated()){
		User.findById(req.user._id, (err, foundUser) =>{
			if(err){
				res.redirect("back");
			}else{
				if(foundUser.accountType === "Personal Trainer"){
					next();
				}else{
					req.flash("error", "Error - you need to be a Personal Trainer to do that.");
					res.redirect("back");
				}
			}
		});
	}else{
		req.flash("error", "Error - you need to be logged in to do that.");
		res.redirect("back");
	}
}

//MIDDLEWARE TO CHECK IF USER IS A CLIENT
middleware.checkIfClient = (req, res, next) =>{
	if(req.isAuthenticated()){
		User.findById(req.user._id, (err, foundUser) =>{
			if(err){
				res.redirect("back");
			}else{
				if(foundUser.accountType === "Client"){
					next();
				}else{
					req.flash("error", "Error - you need to be a Client to do that.");
					res.redirect("back");
				}
			}
		});
	}else{
		req.flash("error", "Error - you need to be logged in to do that.");
		res.redirect("back");
	}
};

//MIDDLEWARE TO CHECK REVIEW OWNERSHIP
middleware.checkReviewOwnership = function(req, res, next) {
    if(req.isAuthenticated()){
        Review.findById(req.params.review_id, function(err, foundReview){
            if(err || !foundReview){
                res.redirect("back");
            }  else {
                // does user own the comment?
                if(foundReview.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "Error - you don't have permission to do that");
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "Error - you need to be logged in to do that");
        res.redirect("back");
    }
};
	
//MIDDLEWARE TO CHECK IF USER HAS REVIEWED TRAINER ALREADY
middleware.checkReviewExistence = function (req, res, next) {
    if (req.isAuthenticated()) {
        User.findById(req.params.id).populate("reviews").exec(function (err, foundPersonalTrainer) {
            if (err || !foundPersonalTrainer) {
                req.flash("error", "Error - personal trainer not found.");
                res.redirect("back");
            } else {
                // check if req.user._id exists in foundPersonalTrainer.reviews
                var foundUserReview = foundPersonalTrainer.reviews.some(function (review) {
                    return review.author.id.equals(req.user._id);
                });
                if (foundUserReview) {
                    req.flash("error", "Error - you already wrote a review.");
                    return res.redirect("/personaltrainers/" + foundPersonalTrainer._id);
                }
                // if the review was not found, go to the next middleware
                next();
            }
        });
    } else {
        req.flash("error", "Error - You need to login first.");
        res.redirect("back");
    }
};






module.exports = middleware;
