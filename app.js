require('dotenv').config();
const express = require("express"),
	  app = express(),
      rp = require("request-promise"),
	  bodyParser = require("body-parser"),
      mongoose = require("mongoose"),
      methodOverride = require("method-override"),
      expressSanitizer = require("express-sanitizer"),
      passport = require("passport"),
      User = require("./models/user.js"),
	  LocalStrategy = require("passport-local"),
	  passportLocalMongoose = require("passport-local-mongoose"),
	  flash = require("connect-flash"),
	  request = require("request"),
	  axios = require("axios"),
	  http = require("http"), 
	  enforce = require("express-sslify") 
	  
//REQUIRING ROUTES
const personalTrainersRoutes = require("./routes/personaltrainers.js"),
	  reviewRoutes = require("./routes/reviews.js"),
	  indexRoutes = require("./routes/index.js"),
	  bookingRoutes = require("./routes/bookings.js"),
	  stripeRoutes = require("./routes/stripe.js")
//SETTING UP PORT
const host = '0.0.0.0';
const port = process.env.PORT || 3000;
//===================================================================================
//LOCAL DATABASE USE TO ADD NEW FEATURES
// mongoose.connect(process.env.LOCAL_DATABASEURL, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true}); 

//ONLINE MONGO ATLAS DATABASE - COMMIT THEN GIT PUSH HEROKU MASTER TO ADD NEW FEATURE TO HEROKU
mongoose.connect(process.env.DATABASEURL, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true});

//===================================================================================
//EXPRESS SESSION
app.use(require("express-session")({
	secret: "12345",
	resave: false,
	saveUninitialized: false
}));
//STRIPE ROUTE & WEBHOOK MIDDELWARE (this must be here in order for stripe webhook to work)
// app.use(stripeRoutes);
// app.use((req, res, next) => {
//   if (req.originalUrl === '/pay-success') {
//     next();
//   } else {
//     express.json()(req, res, next);
//   }
// });
//EXPRESS CONFIG
// app.use(enforce.HTTPS({ trustProtoHeader: true })); 
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static("public"));  
app.use(methodOverride("_method"));
app.use(expressSanitizer());
app.use(flash());
//PASSPORT CONFIG
app.use(passport.initialize());
app.use(passport.session());
	passport.use(new LocalStrategy({usernameField: "email"}, User.authenticate()));
	passport.serializeUser(User.serializeUser());
	passport.deserializeUser(User.deserializeUser());
app.use(function (req, res, next){
	res.locals.currentUser = req.user;
	res.locals.error = req.flash("error");
	res.locals.success = req.flash("success");
	next();
});
//REFACTORED ROUTES
app.use(indexRoutes);
app.use(personalTrainersRoutes);
app.use(bookingRoutes);
app.use(reviewRoutes);
app.use(stripeRoutes);
//===================================================================================
//LISTENER
app.listen(port, host, function() {
  console.log("Server started.......");
});