var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var userSchema = new mongoose.Schema({
	// username: { type: String, unique: true, required: true }, 
	firstName: String,
	lastName: String,
	contactNumber: String,
	email: { type: String, unique: true, required: true },
	password: String,
	profilePicture: { type: String, required: true }, 
	accountType: { type: String, required: true }, 
	address: { type: String, required: true },
	postCode: { type: String, required: true },
	borough: String,
	country: { type: String, required: false},
	aboutMe: String,
	lat: Number,
	lng: Number,
	reviews: [ { type: mongoose.Schema.Types.ObjectId, ref: "Review" } ],
    rating: { type: Number, default: 0 },
	gender: { type: String, required: true}
});

userSchema.plugin(passportLocalMongoose, {usernameField: "email"});
module.exports = mongoose.model("User", userSchema);


	