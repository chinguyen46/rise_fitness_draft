var mongoose = require("mongoose");

var reviewSchema = new mongoose.Schema({
    rating: {
        type: Number,
        required: "Please provide a rating (1-5 stars).",
        min: 1,
        max: 5,
        validate: {
            validator: Number.isInteger,
            message: "{VALUE} is not an integer value."
        }
    },
    text: { type: String },
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        firstName: String
    },
    // campground associated with the review
    personalTrainer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
}, {
   
    timestamps: true
});

module.exports = mongoose.model("Review", reviewSchema);