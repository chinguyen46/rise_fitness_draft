var mongoose = require("mongoose");

var bookingSchema = new mongoose.Schema({
	sessionPaid: { type: Boolean, default: false },
	sessionType: { type: String, required: true },
	sessionLength: { type: String, required: true },
	sessionDate: Date, //note to self when displaying this use .toDateString() to make it more readable for humans
	sessionTime: { type: String, required: true }, //not sure if this is the correct way to persist time data?
	userBookingSession: {
		id:{ type: mongoose.Schema.Types.ObjectId, ref: "User" },
		firstName: String,
		contactNumber: String,
		email: String
	},
	sessionAddress: { type: String, required: true },
	sessionPostCode: { type: String, required: true },
	sessionStatus: String,
	numberOfPeople: Number,
	bookedPersonalTrainer: {
		id: String,
		firstName: String,
		email: String,
		contactNumber: String
	},
});


module.exports = mongoose.model("Booking", bookingSchema);
