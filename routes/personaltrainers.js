const express = require("express"),
 	  router = express.Router(),
	  User = require("../models/user.js"),
	  Review = require("../models/review.js"),
	  middleware = require("../middleware/index.js"),
	  multer = require('multer'),
	  request = require("request")

const storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});
const imageFilter = function (req, file, cb) {
    // accept image files only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
var upload = multer({ storage: storage, fileFilter: imageFilter})
var cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: "dwsmsfuzo", 
  api_key: 441428281642141, 
  api_secret: process.env.CLOUDINARY_API_SECRET
});

//===================================================================================

//SHOW BECOME A PT PAGE
router.get("/personaltrainers/instructions", (req, res) =>{
	res.render("personaltrainers/becomePT.ejs");
});


//SHOW ALL PTS IRRESPECTIVE OF POSTCODE
// router.get("/allpersonaltrainers", (req, res) =>{
// 	User.find({ accountType: "Personal Trainer" }, (err, foundPersonalTrainers) =>{
// 		if(err){
// 			console.log(err);
// 			req.flash("error", "You need to be logged in to do that. If you do not have an account please register.");
// 			res.redirect("/login");
// 		}else{
// 			res.render("personaltrainers/showAll.ejs", {foundPersonalTrainers: foundPersonalTrainers});
// 		}
// 	});
// });


//WHEN USER INPUTS POSTCODE SHOW ALL PTS IN THAT AREA
router.get("/personaltrainers", (req, res) =>{
	const postcode = req.query.postcode; 	
	request("http://api.postcodes.io/postcodes/" + postcode, (err, response, body) => {
	if(!err && response.statusCode == 200){
		var parsedBody = JSON.parse(body);
		var borough = parsedBody.result.admin_district; 
		User.find( { borough: borough, accountType: 'Personal Trainer'}, (err, foundPersonalTrainers) =>{
			if(err){
				console.log(err);
			}else{
		res.render("personaltrainers/showAll.ejs", {foundPersonalTrainers: foundPersonalTrainers, borough: borough});
			}
		});
	}else{
		req.flash("error", "Incorrect postcode.");
		res.redirect("/");
	}
});	
});


//SHOW INDIVIDUAL PT PAGE AFTER CLIENT CLICKS 'MORE INFO'
router.get("/personaltrainers/:id", (req, res) =>{
	User.findById(req.params.id).populate({
		path: "reviews",
		options: {sort: {createdAt: -1}}
		}).exec((err, foundPersonalTrainer) =>{
		if(err){
			req.flash("error", "Unable to find personal trainer - please notify risefitnesslondon@gmail.com");
			res.redirect("/");
		}else{
			res.render("personaltrainers/showOne.ejs", {foundPersonalTrainer: foundPersonalTrainer});
		}
	});
});





module.exports = router; 