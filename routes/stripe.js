const express = require("express"),
	  middleware = require("../middleware/index.js"),
	  router = express.Router(),
      stripe = require('stripe')('sk_test_23O8t0dLUe9s7cZVnObgRzz500m0Xevdc5'),
	  bodyParser = require("body-parser"),
	  Booking = require("../models/booking.js")

// const endpointSecret = process.env.WEBHOOK_SECRET;

//===================================================================================
const charge = (token, amount) =>{
	return stripe.charges.create({
	currency: 'GBP',
	source: token,	
	description: 'test charge',
	amount: amount * 100
	});
};

//Stripe charge api/endpoint
router.post("/charge", async (req, res) =>{
	try{
		let data = await charge(req.body.token.id, 65);
		Booking.findOne({ _id : req.body.bookingId} ,(err, foundBooking) =>{
		if(err){
			console.log(err);
		}else{
			foundBooking.sessionPaid = true; 
			foundBooking.save();
		}
		});
		res.status(202).send("Payment complete");
	}catch(error) {
		console.log(error);
		res.status(500);
	}
});

//GET B0OKING PAYMENT SUCCESS PAGE
router.get("/success", (req, res) =>{
	res.render("stripe/paymentsuccess.ejs");
});

//GET B0OKING PAYMENT FAILURE PAGE
router.get("/cancel", (req, res) =>{
	res.render("stripe/paymentfailure.ejs");
});




//STRIPE WEBHOOK 
// router.post('/pay-success', bodyParser.raw({type: 'application/json'}), (request, response) => {
//   const sig = request.headers['stripe-signature'];

//   let event;

//   try {
//     event = stripe.webhooks.constructEvent(request.body, sig, endpointSecret);
//   } catch (err) {
//     return response.status(400).send(`Webhook Error: ${err.message}`);
//   }

//   // Handle the checkout.session.completed event
//   if (event.type === 'checkout.session.completed') {
//     const session = event.data.object;

//     // Fulfill the purchase...
// 	Booking.findOne({ _id : session.client_reference_id} ,(err, foundBooking) =>{
// 		if(err){
// 			console.log(err);
// 		}else{
// 			foundBooking.sessionPaid = true; 
// 			foundBooking.save();
// 			console.log(foundBooking);
// 		}
// 	});
	 
//   }
//   // Return a response to acknowledge receipt of the event
//   response.json({received: true});
// });


module.exports = router; 