const express = require("express"),
 	  passport = require("passport"),
 	  User = require("../models/user.js"),
	  Booking = require("../models/booking.js"),
 	  middleware = require("../middleware/index.js"),
	  request = require("request")

const router = express.Router();
const api_key = process.env.MAILGUN_API_KEY;
const domain = "rise-fitness.co.uk";
const mailgunHost = "api.eu.mailgun.net";
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain, host: mailgunHost});
const multer = require('multer');
const storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});
const imageFilter = function (req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
var upload = multer({ storage: storage, fileFilter: imageFilter})
var cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: "dwsmsfuzo", 
  api_key: 441428281642141, 
  api_secret: process.env.CLOUDINARY_API_SECRET
});


//GOOGLE MAPS
// var NodeGeocoder = require('node-geocoder');
// var options = {
//   provider: 'google',
//   httpAdapter: 'https',
//   apiKey: process.env.GEOCODER_API_KEY,
//   formatter: null
// };
// var geocoder = NodeGeocoder(options);

//===================================================================================


//SHOW LANDING PAGE
router.get("/", (req, res) =>{
	res.render("landing.ejs");
});

//SHOW REGISTER PAGE FOR CLIENTS
router.get("/registerclient", (req, res)=>{
	res.render("authentication/registerclient.ejs");
});
//SHOW REGISTER PAGE FOR PERSONAL TRAINER
router.get("/registertrainer", (req, res) =>{
	res.render("authentication/registertrainer.ejs")
});

//POST NEW USER INPUT TO SERVER
router.post("/register", upload.single('profilePicture'), (req, res) => {
	geocoder.geocode(req.body.address, (err, data) => {
		if (err || !data.length) { //if user does not provide accurate address this will not work
req.flash("error", "Error: Please ensure you are inputting the correct information. Email risefitnesslondon@gmail.com for assistance.");
			res.redirect('/');
		}else{
			req.body.firstName = req.sanitize(req.body.firstName);
			req.body.lastName = req.sanitize(req.body.lastName);
			req.body.contactNumber = req.sanitize(req.body.contactNumber);
			req.body.email = req.sanitize(req.body.email);
			req.body.postCode = req.sanitize(req.body.postCode);
			req.body.address = req.sanitize(req.body.address);
			//make API request to get information from user postcode
			request("http://api.postcodes.io/postcodes/" + req.body.postCode, (err, response, body) => {
			if(err || response.statusCode == 404){
			req.flash("error", "Incorrect postcode.");
			return res.redirect("/");
				}else{
			//parse JSON returned from API and store into object
			var parsedData = JSON.parse(body);
			var lat = data[0].latitude;
			var lng = data[0].longitude;
			var address = data[0].formattedAddress;
			var newUser = new User({	
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				contactNumber: req.body.contactNumber,
				email: req.body.email,
				profilePicture: req.body.profilePicture,
				accountType: req.body.accountType,
				gender: req.body.gender,
				address: address,
				lat: lat,
				lng: lng,
				postCode: req.body.postCode,
			//get user borough from object
				borough: parsedData.result.admin_district
				});	
			}
				
		cloudinary.uploader.upload(req.file.path, (result) => {
			newUser.profilePicture = result.secure_url;
			User.register(newUser, req.body.password, (err, newUser) => {
				if(err){
					req.flash("error", err.message);
					console.log(err.message);
					res.redirect("/");
				}else{
					passport.authenticate("local")(req, res, function() {
					const data = {
					from: 'Rise Fitness <risefitnesslondon@rise-fitness.co.uk>',
					to: newUser.email,
					subject: 'RISE Fitness registration confirmation',
					text: 'Welcome to the RISE Fitness platform. You have now fully registered.'
					};
					mailgun.messages().send(data, (err, body) => {
						if(err){
						console.log(err);
						}
						console.log("registration confirmation email sent");
						});
						res.redirect("/users/" + newUser._id);
					});	
				}
			});
		});
		});
	}
});
});	
	

//LOGIN FORM FOR USERS
router.get("/login", (req, res) =>{
	res.render("authentication/login.ejs");
});

//POST LOGIN INPUT TO SERVER FOR AUTHENTICATION
router.post("/login", passport.authenticate("local", {
	successRedirect: "/",
	failureRedirect: "/login",
	successFlash: "You have successfully logged in.",
	failureFlash: true }) 
);

//LOGOUT ROUTE
router.get("/logout", (req, res) =>{
	req.logout();
	req.flash("success", "You have successfully logged out.");
	res.redirect("/");
});

//SHOW USERS PROFILES
router.get("/users/:id", middleware.isUserLoggedIn, (req, res) =>{
	User.findById(req.params.id, (err, foundUser) =>{
		if(err){
			console.log(err);
		}else{
	Booking.find({$or: [{"userBookingSession.id" : req.params.id}, {"bookedPersonalTrainer.id": req.params.id}]} , (err, foundBooking) =>{
				if(err){
					console.log(err);
				}else{
					res.render("users/show.ejs", {foundUser: foundUser, foundBooking: foundBooking});
				}
			});
		}
	});
});

//UPDATE USER PROFILE PAGE 
router.put("/users/:id", middleware.isUserLoggedIn, (req, res) =>{
	User.findByIdAndUpdate(req.params.id, { $set: { 
		email: req.body.email,
		profilePicture: req.body.profilePic,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		address: req.body.address,
		postCode: req.body.postCode,
		aboutMe: req.body.aboutMe
	}}, {new: true}, (err, updatedUser) =>{
		if(err){
			console.log(err);
		}else{
			res.status(202).send("Profile updated successfully");
			// res.redirect(303, '/');
		}
	});
});



module.exports = router; 