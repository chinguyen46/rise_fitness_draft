const express = require("express"),
	  router = express.Router(),
	  Booking = require("../models/booking.js"),
	  User = require("../models/user.js"),
	  middleware = require("../middleware/index.js")
	  // sendEmail = require("./mailgun.js")

const api_key = process.env.MAILGUN_API_KEY;
const domain = "rise-fitness.co.uk";
const mailgunHost = "api.eu.mailgun.net";
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain, host: mailgunHost});

const multer = require('multer');
const storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});
const imageFilter = function (req, file, cb) {
    // accept image files only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
var upload = multer({ storage: storage, fileFilter: imageFilter})
var cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: "dwsmsfuzo", 
  api_key: 441428281642141, 
  api_secret: process.env.CLOUDINARY_API_SECRET
});

//===================================================================================


//POST BOOKING DATA 
router.post("/personaltrainers/:id/bookings", middleware.isUserLoggedIn, (req, res) =>{
	User.findById(req.params.id, (err, foundPersonalTrainer) =>{
		if(err){
			console.log(err);
		}else{
			req.body.booking.sessionAddress = req.sanitize(req.body.booking.sessionAddress);
			req.body.booking.sessionPostCode = req.sanitize(req.body.booking.sessionPostCode);
			Booking.create(req.body.booking, (err, newBooking) =>{
				if(err){
					console.log(err);
				}else{
				newBooking.bookedPersonalTrainer.id = foundPersonalTrainer._id;
				newBooking.bookedPersonalTrainer.firstName = foundPersonalTrainer.firstName;
				newBooking.bookedPersonalTrainer.contactNumber = foundPersonalTrainer.contactNumber;
				newBooking.bookedPersonalTrainer.email = foundPersonalTrainer.email;
				newBooking.sessionStatus = "Pending";
				newBooking.userBookingSession.id = req.user._id; //gets user who is logged in ID
				newBooking.userBookingSession.firstName = req.user.firstName; 
				newBooking.userBookingSession.email = req.user.email;
				newBooking.userBookingSession.contactNumber = req.user.contactNumber;
				newBooking.save();
					
				const data = {
				from: 'Rise Fitness <risefitnesslondon@rise-fitness.co.uk>',
				to: newBooking.userBookingSession.email,
				subject: 'RISE Fitness booking confirmation',
				text: 'Hi this is to confirm your session has now been booked and is pending approval from the personal trainer. You will be notified if it has been approved.'
				};
				mailgun.messages().send(data, (err, body) => {
				if(err){
					console.log(err);
				}
					console.log("Booking confirmation email sent");
				});	
					req.flash("success", "Booking successfully made.");
					res.redirect("/bookingconfirmation");
				}
			});
		}
	});
});

//UPDATE BOOKING SESSION STATUS
router.put("/bookings/:id", middleware.isUserLoggedIn, (req, res) =>{
Booking.findByIdAndUpdate(req.params.id, { $set: { sessionStatus: req.body.confirmation }}, {new: true}, (err, updatedBooking) =>{
		if(err){
			console.log(err);
		}else if(updatedBooking.sessionStatus === 'Rejected'){
				const data = {
				from: 'Rise Fitness <risefitnesslondon@rise-fitness.co.uk>',
				to: updatedBooking.userBookingSession.email,
				subject: 'RISE fitness booking update',
				text: 'Hi this is to confirm your session has been rejected.'
				};
				mailgun.messages().send(data, (err, body) => {
				if(err){
					console.log(err);
				}
				res.status(202).send("Booking status updated to rejected - email sent");
				});	
		}else{
				const data = {
				from: 'Rise Fitness <risefitnesslondon@rise-fitness.co.uk>',
				to: updatedBooking.userBookingSession.email,
				subject: 'RISE fitness booking update',
				text: 'Hi this is to confirm your session has been accepted.'
				};
				mailgun.messages().send(data, (err, body) => {
				if(err){
					console.log(err);
				}
				res.status(202).send("Booking status updated to accepted - email sent");
			});	
		}	
	});
});


//GET CONFIRMATION OF BOOKING PAGE
router.get("/bookingconfirmation", middleware.isUserLoggedIn, (req, res) =>{ 
	res.render("bookings/confirmation.ejs", {currentUser: req.user});
});


//SEND JSON DATA FOR BOOKINGS OF USER 
router.get("/booking/:id", middleware.isUserLoggedIn, (req, res) =>{
	User.findById(req.params.id, (err, foundUser) =>{
		if(err){
			console.log(err);
		}else{
	Booking.find({$or: [{"userBookingSession.id" : req.params.id}, {"bookedPersonalTrainer.id": req.params.id}]} , (err, foundBookings) =>{
				if(err){
					console.log(err);
				}else{
					res.json({foundBookings, foundUser}); //send booking data and user data
				}
			});
		}
	});
});





module.exports = router; 