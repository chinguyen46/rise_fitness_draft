const express = require("express"),
	  router = express.Router(),
	  User = require("../models/user.js"),
	  Review = require("../models/review.js"),
	  middleware = require("../middleware/index.js")

//===================================================================================

// GET REVIEW INDEX ROUTE WHICH SHOWS ALL REVIEWS ASSOCIATED WITH PERSONAL TRAINER
router.get("/personaltrainers/:id/reviews", function (req, res) {
    User.findById(req.params.id).populate({
        path: "reviews",
        options: {sort: {createdAt: -1}} // sorting the populated reviews array to show the latest first
    }).exec(function (err, foundPersonalTrainer) {
        if (err || !foundPersonalTrainer) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
        res.render("reviews/index.ejs", {foundPersonalTrainer: foundPersonalTrainer});
    });
});


// GET NEW REVIEW PAGE - CHECK IF USER IS LOGGED IN / HAS REVIEWED PREVIOUSLY
router.get("/personaltrainers/:id/reviews/new", middleware.checkIfClient, middleware.checkReviewExistence, function (req, res) {
	// middleware.checkReviewExistence checks if a user already reviewed the trainer, only one review per user is allowed
    User.findById(req.params.id, function (err, foundPersonalTrainer) {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
        res.render("reviews/new.ejs", {foundPersonalTrainer: foundPersonalTrainer});
    });
});


// POST NEW REVIEW COMMENT TO DATABASE
router.post("/personaltrainers/:id/reviews", middleware.checkIfClient, middleware.checkReviewExistence, function (req, res) {
	//lookup personaltrainer using ID
    User.findById(req.params.id).populate("reviews").exec(function (err, foundPersonalTrainer) {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
     	req.body.review.text = req.sanitize(req.body.review.text); //sanitize input
        Review.create(req.body.review, function (err, review) {
            if (err) {
                req.flash("error", err.message);
                return res.redirect("back");
            }
	       // username/id and associated personal trainer to the review
            review.author.id = req.user._id;
            review.author.firstName = req.user.firstName;
            review.personalTrainer = foundPersonalTrainer;
            review.save();
            foundPersonalTrainer.reviews.push(review);
            // calculate the new average review for the personal trainer
            foundPersonalTrainer.rating = calculateAverage(foundPersonalTrainer.reviews);
            //save review to db
            foundPersonalTrainer.save();
            req.flash("success", "Review successfully added.");
            res.redirect('/personaltrainers/' + foundPersonalTrainer._id);
        });
    });
});

// GET REVIEWS EDIT PAGE
router.get("/personaltrainers/:id/reviews/:review_id/edit", middleware.checkReviewOwnership, function (req, res) {
    Review.findById(req.params.review_id, function (err, foundReview) {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
        res.render("reviews/edit.ejs", {foundPersonalTrainer_id : req.params.id, review: foundReview});
    });
});


// UPDATE EDITED REVIEW
router.put("/personaltrainers/:id/reviews/:review_id", middleware.checkReviewOwnership, function (req, res) {
    Review.findByIdAndUpdate(req.params.review_id, req.body.review, {new: true}, function (err, updatedReview) {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
        User.findById(req.params.id).populate("reviews").exec(function (err, foundPersonalTrainer) {
            if (err) {
                req.flash("error", err.message);
                return res.redirect("back");
            }
            // recalculate personal trainer review average
            foundPersonalTrainer.rating = calculateAverage(foundPersonalTrainer.reviews);
            //save changes
            foundPersonalTrainer.save();
            req.flash("success", "Review successfully edited.");
            res.redirect('/personaltrainers/' + foundPersonalTrainer._id);
        });
    });
});

// DELETE REVIEW
router.delete("/personaltrainers/:id/reviews/:review_id", middleware.checkReviewOwnership, function (req, res) {
    Review.findByIdAndRemove(req.params.review_id, function (err) {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
        }
        User.findByIdAndUpdate(req.params.id, {$pull: {reviews: req.params.review_id}}, {new: true}).populate("reviews").exec(function (err, foundPersonalTrainer) {
            if (err) {
                req.flash("error", err.message);
                return res.redirect("back");
            }
            // recalculate campground average
            foundPersonalTrainer.rating = calculateAverage(foundPersonalTrainer.reviews);
            //save changes
            foundPersonalTrainer.save();
            req.flash("success", "Review successfully deleted.");
            res.redirect("/personaltrainers/" + req.params.id);
        });
    });
});

//function which updates the personaltrainer.rating field
function calculateAverage (reviews) {
    if (reviews.length === 0) {
        return 0;
    }
    var sum = 0;
    reviews.forEach(function (element) {
        sum += element.rating;
    });
    return sum / reviews.length;
}

module.exports = router; 