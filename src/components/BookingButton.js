import React from 'react';
import StripeCheckoutButton from './StripeCheckoutButton.js';



const BookingButton = ({ bookingPaid, accountType, bookingStatus, onClick, bookingId }) =>{
	
switch (true){
	case (bookingStatus === 'Pending' && accountType === 'Client'):
		return (
		<div className="container fluid mt-4 text-center">
			<span className="text-secondary"><i className="fas fa-clock"></i> Session pending approval</span>
		</div>
		)
	case (bookingStatus === 'Accepted' && !bookingPaid && accountType === 'Client'):
		return (
		<div className="container fluid mt-4 text-center">
			<StripeCheckoutButton bookingId={bookingId} price={65}/>
		</div>
		)
	case (bookingStatus === 'Accepted' && bookingPaid && accountType === 'Client'):
		return(
		<div className="container fluid mt-4 text-center">
			<span className="text-success"><i className="fas fa-check-circle"></i> Payment successful</span>
		</div>
		)
		break;
	case (bookingStatus === 'Rejected' && accountType === 'Client'):
		return (
		<div className="container fluid mt-4 text-center">
			<span className="text-danger"><i className="fas fa-times-circle"></i> Session rejected by trainer</span>
		</div>
		)
	case (accountType === 'Personal Trainer' && bookingStatus === 'Accepted' && !bookingPaid):
		return(
		<div className="container fluid mt-4 text-center">
			<span className="text-secondary"><i className="fas fa-clock"></i> Accepted - pending payment</span>
		</div>
		)
	case (accountType === 'Personal Trainer' && bookingPaid && bookingStatus ==='Accepted'):
		return(
		<div className="container fluid mt-4 text-center">
			<span className="text-success"><i className="fas fa-check-circle"></i> Booking has been paid</span>
		</div>
		)
	case (accountType === 'Personal Trainer' && bookingStatus === 'Rejected'):
		return(
		<div className="container fluid mt-4 text-center">
			<span className="text-danger"><i className="fas fa-times-circle"></i> You rejected this booking</span>
		</div>
		)
	default:
		return(
		<div className="container fluid mt-4 text-center">
			<button onClick={onClick} value="accept" className="btn btn-accept mr-1">Accept</button>
			<button onClick={onClick} value="reject" className="btn btn-reject ml-1">Reject</button>
		</div>
		)	
	}
};

export default BookingButton;