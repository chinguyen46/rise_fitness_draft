import React from 'react';
import BookingItem from './BookingItem.js';

const BookingList = ({ foundUser, bookings }) =>{
	
	const renderedList = bookings.map((booking) =>{
		return <BookingItem 
				   booking={booking}
				   key={booking._id}
				   foundUser={foundUser}
				   />;
	});

	return (
		<div className="container-fluid pt-2 pb-8 pt-md-8">
			<div className="row">
				{renderedList}
			</div>	
		</div>	
	)
};

export default BookingList; 