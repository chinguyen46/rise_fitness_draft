import React from 'react';
import BookingButton from './BookingButton.js';
import axios from 'axios';

class BookingItem extends React.Component{
	state={
		bookingStatus: this.props.booking.sessionStatus,
		paymentStatus: this.props.booking.sessionPaid
	}
	
	onButtonClick = (event) =>{
		var button = event.target; 
		var requestPayload = {}; //requestPayload === req.body
		var bookingID = this.props.booking._id;
		var config = {headers: {'content-type': 'application/json'}};
		var url = '/bookings/' + bookingID;
	
		if(button.value === 'accept'){
			requestPayload.confirmation = 'Accepted';
		}else if(button.value === 'reject'){
			requestPayload.confirmation = 'Rejected';
		}
		
		axios.put(url, requestPayload, config).
		then(() =>{
			this.setState({bookingStatus: requestPayload.confirmation});
		}).catch((error) =>{
			console.log(error);
		});
		
	};

	
	infoForTrainer(){
	return (
			<div className="card-body-2">
				<div className="row">
					<div className="col-lg-12">
	<span className="card-title text-center">Status: {this.state.bookingStatus}</span>
						<br />
	<h5 className="text-muted mt-3"><strong>Exercise: </strong>{this.props.booking.sessionType}</h5>
						<br />
	<h5 className="text-muted"><strong>Location: </strong>{this.props.booking.sessionAddress}</h5>
						<br />
	<h5 className="text-muted"><strong>Time: </strong>{this.props.booking.sessionTime}</h5>
						<br />
	<h5 className="text-muted"><strong>Cient Name: </strong>{this.props.booking.userBookingSession.firstName}</h5>
						<br />
<h5 className="text-muted"><strong>Client Contact: </strong>{this.props.booking.userBookingSession.contactNumber}</h5>
						<br />					
	<h5 className="text-muted"><strong>Duration: </strong>{this.props.booking.sessionLength}</h5>
						<br />
	<h5 className="text-muted"><strong>No. of people: </strong>{this.props.booking.numberOfPeople}</h5>
					</div>
					<div className="col-auto col"></div>		
				</div>
				<div className="mt-3">
					<BookingButton 
						bookingId={this.props.booking._id}
						bookingStatus={this.state.bookingStatus}
						bookingPaid={this.state.paymentStatus}
						accountType={this.props.foundUser.accountType}
						onClick={this.onButtonClick} 
					/>
				</div>
			</div>
		)
	};

	infoForClient(){
	return (
			<div className="card-body-2">
				<div className="row">
					<div className="col-lg-12">
	<span className="card-title text-center">Status: {this.props.booking.sessionStatus}</span>
						<br />
	<h5 className="text-muted mt-3"><strong>Exercise: </strong>{this.props.booking.sessionType}</h5>
						<br />
	<h5 className="text-muted"><strong>Location: </strong>{this.props.booking.sessionAddress}</h5>
						<br />
	<h5 className="text-muted"><strong>Time: </strong>{this.props.booking.sessionTime}</h5>
						<br />
	<h5 className="text-muted"><strong>Trainer Name: </strong>{this.props.booking.bookedPersonalTrainer.firstName}</h5>
						<br />
<h5 className="text-muted"><strong>Trainer Contact: </strong>{this.props.booking.bookedPersonalTrainer.contactNumber}</h5>
						<br />
	<h5 className="text-muted"><strong>Duration: </strong>{this.props.booking.sessionLength}</h5>
						<br />
	<h5 className="text-muted"><strong>No. of people: </strong>{this.props.booking.numberOfPeople}</h5>
					</div>
					<div className="col-auto col"></div>
				</div>
				<div className="mt-3">
					<BookingButton 
						bookingId={this.props.booking._id}
						bookingStatus={this.state.bookingStatus}
						bookingPaid={this.state.paymentStatus}
						accountType={this.props.foundUser.accountType}
						onClick={this.onButtonClick} 
					/>
				</div>
			</div>
		)
	}


	render(){
		if(this.props.foundUser.accountType === 'Client'){
			return <div className=" col-xl-3 col-lg-6 col-md-6 col-sm-6 mt-3"> {this.infoForClient()} </div>
		}else{
			return <div className="col-xl-3 col-lg-6 col-md-6 col-sm-6 mt-3"> {this.infoForTrainer()} </div>
		}
	}
}

export default BookingItem; 