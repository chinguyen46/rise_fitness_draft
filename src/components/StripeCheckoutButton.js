import React from 'react';
import StripeCheckout from 'react-stripe-checkout';

const StripeCheckoutButton = ({ price, bookingId }) =>{
	
	const priceForStripe = price * 100;
	const publishableKey = 'pk_test_PuyEqXmAi35KvX74soFFleOS005o69IFQP';
	
	const onToken = async (token) =>{
	try {
		let config = {headers: {'content-type': 'application/json'}};
		let url = '/charge';
		let requestPayload = {};
			requestPayload.bookingId = bookingId;
			requestPayload.token = token;
		//make post request to server endpoint /charge to update db
		await axios.post(url, requestPayload, config);
		await alert("Payment Completed");
		window.location.reload();
	}catch(error){
		console.log(error);
	}	
	};
	
	return(
		<StripeCheckout 
		label='Pay Now'
		name='RISE'
		currency='GBP'
		image='https://www.sectorsdonut.co.uk/sites/default/files/Sectors/PersonalTrainer.jpg'
		description={'RISE - Pay as you go session'}
		amount={priceForStripe}
		panelLabel='Pay Now'
		token={onToken}	
		stripeKey={publishableKey}
		/>
	);
};

export default StripeCheckoutButton;