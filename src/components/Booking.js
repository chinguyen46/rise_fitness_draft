import React from 'react';
import BookingList from './BookingList.js';
import axios from 'axios';


class Booking extends React.Component{
	state = {
		bookings: [],
		userInfo: null
	};
	

	getBookingData = async () =>{
		try{
			var url = window.location.pathname; 
			var userId = url.substring(url.lastIndexOf('/') + 1);
			const response = await axios.get('/booking/' + userId);
			this.setState({
				bookings: response.data.foundBookings,
				userInfo: response.data.foundUser
			});
		} catch (error){
		console.log(error);
		}
	}
	
	componentDidMount(){
		this.getBookingData();
	}

	render() {
		return(
			<BookingList bookings={this.state.bookings} foundUser={this.state.userInfo} />
		);
	}
}

export default Booking;
