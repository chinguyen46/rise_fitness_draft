//update profile 
	updateProfile = (event) =>{

	event.preventDefault();
	var button = event.target;
	var userId = button.id; 
	var config = {headers: {'content-type': 'application/json'}};
	var url = '/users/' + userId;
	var requestPayload = {};
	requestPayload.email = document.getElementById('input-email').value;
	requestPayload.profilePic = document.getElementById('input-profile-pic').value;
	requestPayload.firstName = document.getElementById('input-first-name').value;
	requestPayload.lastName = document.getElementById('input-last-name').value;
	requestPayload.address = document.getElementById('input-address').value;
	requestPayload.postCode = document.getElementById('input-postal-code').value;
	requestPayload.aboutMe = document.getElementById('input-about-me').value;

	axios.put(url, requestPayload, config).
	then(() =>{
		button.textContent = "Updated!";
	}).
	then(() =>{
		setTimeout(() =>{
			window.location.reload();
		}, 1500);
	}).
	catch((error) =>{
		console.log(error);
	});
	}
	
	var updateButton = document.querySelector('.update-button');
	updateButton.addEventListener('click', updateProfile); 
	
