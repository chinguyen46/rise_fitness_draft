const path = require("path");


module.exports = {
	entry: './src/index.js',
	output:{
		path: path.join(__dirname, '/public/dist/'),
		filename: 'bundle.js'
	},
	watch: false,
	module:{
		rules: 
			[{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			use: ['babel-loader']	 
			}]
		},
	devServer: {
		host: '0.0.0.0',
		port: 3000,
		compress: true,
  		public: 'chi-3-jfvyb.run.goorm.io'
	},
	devtool: 'inline-source-map'
};
